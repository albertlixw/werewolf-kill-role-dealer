import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Werewolf_card_drawer_2021_FEB_13 {


    public static void main(String[] args) {
        ArrayList<String> roles = new ArrayList<>();
        roles.add("预言家");
        roles.add("女巫");
        roles.add("猎人");
        roles.add("守卫");

        roles.add("白狼王");


        for(int i = 0; i<3; i++){
            roles.add("狼人");
        }

        roles.add("吹笛者");

        for(int i = 0; i<4; i++){
            roles.add("平民");
        }

        Collections.shuffle(roles);

        for(int i = 0; i < roles.size(); i++){
            System.out.println(i+1 + " : " + roles.get(i));
        }

    }
}
